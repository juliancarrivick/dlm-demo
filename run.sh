#!/bin/bash

# Run each of these, in order, in different terminal windows

docker compose -p dlm_demo up
docker compose -p dlm_demo exec -it kafka kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic ska-sdp-dataproduct-ingest
# For manual messages:
# docker compose -p dlm_demo exec -it kafka kafka-console-producer.sh --bootstrap-server localhost:9092 --topic ska-sdp-dataproduct-ingest

docker run \
    -v ./shared:/tmp/shared \
    --network=dlm_demo_default \
    artefact.skao.int/ska-sdp-cbf-emulator:7.0.0 \
    emu-send \
        -o ms.location=/tmp/shared/AA05LOW.ms \
        -o transmission.num_streams=1 \
        -o transmission.transport_protocol=tcp \
        -o transmission.target_host=receiver
