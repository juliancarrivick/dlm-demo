#!/bin/bash

mkdir shared
chmod 777 shared
pushd shared
curl -L https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-modules/-/raw/92e7264ad7673c75acb825e1c73b0522adca1440/data/AA05LOW.ms.tar.gz | tar xz
popd
